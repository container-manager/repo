#!/bin/bash

apt install -y apt-transport-https

curl -L https://container-manager.gitlab.io/repo/gpgkey | apt-key add -
echo "deb https://container-manager.gitlab.io/repo ./" > /etc/apt/sources.list.d/container-manager.list
